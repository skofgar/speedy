# docker run --rm  speedtest
# docker run -d --rm -v $(pwd)/data:/data speedtest
# docker run -d --restart=always -v $(pwd)/data:/data speedtest
# docker build . --tag speedtest:latest
# docker build . --tag speedtest:latest --platform linux/arm64
FROM arm64v8/ubuntu:22.04

# RUN apt-get update -y && \
#     apt-get upgrade -y
# RUN apt-get install gnupg1 apt-transport-https  -y
RUN apt-get update -y && \
    apt-get install dirmngr curl dnsutils -y

RUN curl -s https://packagecloud.io/install/repositories/ookla/speedtest-cli/script.deb.sh | bash
RUN apt-get install speedtest

######
# Configure Cronjob
RUN apt-get -y install cron

# Copy hello-cron file to the cron.d directory
COPY speedtest-cron /etc/cron.d/speedtest-cron

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/speedtest-cron

# Apply cron job
RUN crontab /etc/cron.d/speedtest-cron

# Create the log file to be able to run tail
RUN mkdir -p /data
RUN touch /data/cron.log


#CMD speedtest -f csv --output-header --accept-license > /data/speedtest.csv
RUN mkdir -p /scripts
COPY do-speedtest.sh /scripts/
RUN chmod 0744 /scripts/do-speedtest.sh
CMD  touch /data/cron.log && cron && tail -f /data/cron.log
