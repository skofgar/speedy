#!/bin/bash

CURRENT_DATE=$(date --iso-8601=seconds)

if [ -z "$1" ]; then
   SPEED_MEASUREMENT=$(speedtest -f csv --accept-license)
elif [ "$1" == "output-header" ]; then
   SPEED_MEASUREMENT=$(speedtest -f csv --output-header --accept-license)
else
   echo "invalid command `$1`. Leave empty or use `output-header`"
fi

echo -e $CURRENT_DATE,$SPEED_MEASUREMENT >> /data/speedtest.csv
