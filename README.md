# Setup

## Raspberry

Configure your raspberry os pi with:

```bash
# update raspberry pi os
sudo apt-get update -y && sudo apt-get upgrade -y

# install docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# update user permissions
# add non-root user to be able to run docker
sudo usermod -aG docker $USER

# restart
sudo shutdown -r now

# verify installation
# start service
sudo systemctl start docker
# CTRL+C to end
docker run hello-world

# enable start of docker on startup
sudo systemctl start docker
```

## Start docker image

```bash
# create folder to store data
$DATA_FOLDER="data"
mkdir -p "${DATA_FOLDER}"

IMAGE_URL=registry.gitlab.com/skofgar/speedy:snapshot
docker run -d --restart unless-stopped -v "$(pwd)/${DATA_FOLDER}:/data" "${IMAGE_URL}"
```

After a few minutes the speedtest.csv should be created.

## Watch data

Watch the data by following the file:

```bash
tail -f data/speedtest.csv
```
